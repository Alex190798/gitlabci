interface IUser {
    name: string;
    age: number;
}

/**
 * Класс пользователя
 *
 * ```ts
 * // Пример объявления
 * const instance = new User({name: "Aleksandr", age: 24});
 * ```
 * 
 * @category Model
 */
class User implements IUser {
    public readonly name: string;
    public readonly age: number;

    constructor(params: IUser) {
        this.name = params.name;
        this.age = params.age;
    }
}

interface ICat {
    nickname: string;
    age: number;
}

class Cat {
    public readonly nickname: string;
    public readonly age: number;

    constructor(params: ICat) {
        this.nickname = params.nickname;
        this.age = params.age;
    }
}


interface IGretter {
    name: string;
}


function greet(someone: IGretter) {
    console.log(`Hi, ${someone.name}`);
}



greet(new User({ name: 'Alex', age: 20}));

function nop() {
    const unused = 'noop';
    console.log('foo');
}