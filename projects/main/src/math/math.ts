/**
 * Возвращает максимальное число из двух
 * 
 * ```ts
 * // Пример использования
 * const result = greater(10, 5);
 * 
 * // Возвращает 10
 * console.log(result);
 * ```
 *
 * @param a первый операнд
 * @param b второй операнд
 * @returns наибольший из операндов
 */
export function greater(a: number, b: number) {
    if (a > b) {
        return a;
    }

    return b;
}