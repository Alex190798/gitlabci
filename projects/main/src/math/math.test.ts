import { greater } from "./math"

describe('when first number is greater than second', () => { 
    it('should return first number', () => {
        expect(greater(10, 2)).toBe(10);
    })
 })